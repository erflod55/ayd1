const express = require('express');
const app = express();

app.set('port', process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded({extended : false}));

app.get('/', (req,res)=>{
    res.send({message:"Hola mundo Cambio"});
})

app.listen(app.get('port'), ()=>{
    console.log(`Server running on port ${app.get('port')}`);
});
